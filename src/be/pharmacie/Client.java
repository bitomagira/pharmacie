package be.pharmacie;

import java.nio.charset.Charset;

public class Client {
	
	
	private String nomClient;
	private double credit;
	
	
	
	
	public Client(String nomClient, double credit) {
		super();
		this.nomClient = nomClient;
		this.credit = credit;
	}
	
	@Override
	public String toString() {
		return " \r\nClient=" + nomClient + ", credit=" + credit;
	}
	
	public String getNomClient() {
		return nomClient;
	}
	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}
	public double getCredit() {
		return credit;
	}
	public void setCredit(double credit) {
		this.credit = credit;
	}

	public byte[] getBytes(Charset forName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	

}
