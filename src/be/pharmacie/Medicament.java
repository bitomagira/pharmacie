package be.pharmacie;

public class Medicament {
	
	
	private String nomMedicament;
	private double prix;
	private int stock;
	
	public Medicament(String nomMedicament, double prix, int stock) {
		super();
		this.nomMedicament = nomMedicament;
		this.prix = prix;
		this.stock = stock;
	}
	
	@Override
	public String toString() {
		return "\r\n Nom du médicament: " + nomMedicament + "; prix: " + prix + "; stock: " + stock + "]";
	}
	
	public String getNomMedicament() {
		return nomMedicament;
	}
	public void setNomMedicament(String nomMedicament) {
		this.nomMedicament = nomMedicament;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}

}
