package be.pharmacie;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


public class Pharmacie {
	
	ArrayList<Client> listeClient = new ArrayList<Client>();
	ArrayList<Medicament> listeMedicament = new ArrayList<Medicament>();
	Path pathClient = Paths.get("src", "be", "pharmacie", "clients.txt");
	Path pathMedicament = Paths.get("src", "be", "pharmacie","medicaments.txt");
	
	
	public void AjoutClient() throws IOException {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Nom du client:  ");
		String nomClient = sc.nextLine();
		
		Client unClient = new Client(nomClient, 0);   // 0 correspond au cr�dit de d�part du client
		listeClient.add(unClient);
		System.out.println("Le client " + unClient.getNomClient() + " a �t� ajout� avec un cr�dit de " + unClient.getCredit() + ".");
		Files.write(pathClient, unClient.toString().getBytes(Charset.forName("ISO_8859_1")),StandardOpenOption.APPEND);
	}
	
	public void AjoutMedicament(String resp) throws IOException {
		
		Scanner sc = new Scanner(System.in);
		if (resp.contentEquals("")) {
			System.out.println("Nom du m�dicament:  ");
			String nomMedicament = sc.nextLine();
			System.out.println("Prix du m�dicament: ");
			double prix = sc.nextDouble();
			System.out.println("Quantit�: ");
			int quantite = sc.nextInt();
			Medicament unMedicament = new Medicament(nomMedicament, prix, quantite);
			listeMedicament.add(unMedicament);
			Files.write(pathMedicament, unMedicament.toString().getBytes(Charset.forName("ISO_8859_1")),StandardOpenOption.APPEND);
		}
		else {
		System.out.println("Prix du m�dicament: ");
		double prix = sc.nextDouble();
		System.out.println("Quantit�: ");
		int quantite = sc.nextInt();
		Medicament unMedicament = new Medicament(resp, prix, quantite);
		listeMedicament.add(unMedicament);
		Files.write(pathMedicament, unMedicament.toString().getBytes(Charset.forName("ISO_8859_1")),StandardOpenOption.APPEND);
		}
		System.out.println(listeMedicament.toString());
	}
	
	public void Approvisionner() throws IOException {
		
		Scanner sc = new Scanner(System.in);
		String check = sc.nextLine();
		int result = 0;
		for (Medicament el : getListeMedicament()) {
			if (el.getNomMedicament().contentEquals(check)) { result = 1; }
			else { result = 0; }
				
		}
		// Si le m�dicament n'existe pas:
		
		if (result == 0) {
			System.out.println("m�dicament non cr��");
			System.out.println("Souhaitez-vous le cr�er? ");
			Scanner sc1 = new Scanner(System.in);
			String resp1 = sc1.nextLine();
			if (resp1.equals("Y")) {
				System.out.println("coucou");
				AjoutMedicament(check);
			}	
			
		}	
		// si le m�dicament existe
		else {
			System.out.println("m�dicament trouv�");
		}
		
	}


	//@Override
	public String toStringCli() {
		return "Liste des clients : " + getListeClient() ;
	}	

//	@Override
	public String toStringMed() {
		return "Liste des m�dicaments : " + getListeMedicament();
	}

	public ArrayList<Client> getListeClient() {
		return listeClient;
	}
	public void setListeClient(ArrayList<Client> listeClient) {
		this.listeClient = listeClient;
	}
	public ArrayList<Medicament> getListeMedicament() {
		return listeMedicament;
	}
	public void setListeMedicament(ArrayList<Medicament> listeMedicament) {
		this.listeMedicament = listeMedicament;
	}
	


}
